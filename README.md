# Relatives plugin

![screenshot](https://gitlab.com/francoisjacquet/Relatives/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/plugins/relatives/

Version 10.2 - January, 2024

Author François Jacquet

Sponsored by Asian Hope

License GNU/GPLv2 or later

## Description

This RosarioSIS plugin shows a listing of a student's brothers and sisters (siblings) and parents. That is to say, students who are associated to the same parent(s) user. Consult the "Relatives" tab from the _Student Info_ program.

Note: You can translate the "Relatives" tab and add fields to it using the _Students > Student Fields_ program.

Translated in [French](https://www.rosariosis.org/fr/plugins/relatives/), [Spanish](https://www.rosariosis.org/es/plugins/relatives/), Slovenian and Portuguese (Brazil).

## Content

Students

- Student Info: Relatives tab.

## Install

Copy the `Relatives/` folder (if named `Relatives-master`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Requires RosarioSIS 4.5+
